package main;

import java.io.FileNotFoundException;

import openfile.*;

import openfile.Input;

public class Run {
	
	public static void main(String[] args) throws FileNotFoundException {
		Input input = new Input();
		input.process();
		FileReader fileReader = new FileReader();
		fileReader.setFile(input.getFilePath());
	}

}
