package main;

import java.io.IOException;

import openfile.FileReader;
import openfile.Input;

public class Demo {

	public static void main(String[] args) {
		try {
			Input input = new Input();
			input.process();
			
			FileReader file = new FileReader();
			file.setFile(input.getFilePath());
			System.out.println(file.getContent());
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

}
