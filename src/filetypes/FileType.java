package filetypes;
import java.util.List;
import datastructure.*;

public abstract class FileType {
	
	protected List<Employee> companies;
	
	protected FileType() {
		
	}
	
	protected abstract void populateData(String content) throws Exception;
	
	public List<Employee> getCompanies(){
		return this.companies;
	}

}
