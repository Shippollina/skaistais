package filetypes;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import datastructure.*;
import datastructure.Employee;

public class XML extends FileType {

	@Override
	public void populateData(String content) throws Exception {
		Document xmlDocument = this.convertToXMLDocument(content);
		this.processXMLDocument(xmlDocument);
	}

	private Document convertToXMLDocument(String xmlString) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlString)));
	}

	private void processXMLDocument(Document xmlDocument) {
		Element company = xmlDocument.getDocumentElement();

		this.companies = new ArrayList<Employee>();

		NodeList employeesElements = company.getElementsByTagName("Employee");

		for (int i = 0; i < employeesElements.getLength(); i++) {
			Element employeeElement = (Element) employeesElements.item(i);
			NodeList employeeParameters = employeeElement.getChildNodes();
			
			Employee employee = new Employee();
			for (int j = 0; j < employeeParameters.getLength(); j++) {
				Node parameter = employeeParameters.item(j);
				switch (parameter.getNodeName()) {
				case "firstname":
					employee.setFirstname(parameter.getTextContent());
					break;
				case "lastname":
					employee.setLastname(parameter.getTextContent());
					break;
				case "contactNomber":
					employee.setContactNumber(Integer.parseInt(parameter.getTextContent()));
					break;
				case "email":
					employee.setContactNumber(Integer.parseInt(parameter.getTextContent()));
					break;
				case "address": {
					Address address = new Address();
					NodeList addressParams = ((Element) parameter).getChildNodes();

					for (int k = 0; k < addressParams.getLength(); k++) {
						Node addressParam = addressParams.item(k);
						switch (addressParam.getNodeName()) {
						case "city":
							address.setCity(addressParam.getTextContent());
							break;
						case "state":
							address.setState(addressParam.getTextContent());
							break;
						case "zip":
							address.setZip(Integer.parseInt(addressParam.getTextContent()));
							break;
						default:
							break;
						}
					}
					break;
				}

				default:
					break;
				}
			}
		}
	}

}
