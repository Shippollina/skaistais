package openfile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader {

	private BufferedReader reader;

	public void setFile(String filepath) throws FileNotFoundException {
		File file = new File(filepath);
		reader = new BufferedReader(new java.io.FileReader(file));
	}

	public String getContent() throws IOException {
		StringBuffer content = new StringBuffer();
		String line = this.reader.readLine();
		while (line != null) {
			content.append(line);
			line = this.reader.readLine();
		}

		return content.toString();
	}

}


//class FileDoesNotExist extends Exception{
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	
//	public FileDoesNotExist(String filepath) {
//		this.filepath = filepath;
//	}
//}